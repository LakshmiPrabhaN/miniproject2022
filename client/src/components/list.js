import React from "react";
import {Table, Row, Col, Button} from "reactstrap";


export default props => {
    return(
        <div>
            <Row noGutters className="text-center align-items-center pizza-cta">
            <Col>
                <p className="looking-for-pizza">
                    Book a Restaurant
                </p>
            </Col>
            </Row>
            <Row noGutters className="text-center big-img-container">
                <Col>
                    <Table hover>
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Restaurant Details
                            </th>
                            <th>
                                
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">
                                1
                            </th>
                            <td>
                                Name: RHR<br></br>
                                City: Coimbatore<br></br>
                                Cuisine: Indian<br></br>
                                Phone number: 1234567890
                            </td>
                            <td>
                                <Button
                                    className="book-table-btn"
                                    onClick={_ => {
                                    props.setPage(1);
                                    }}
                                >
                                    Book Now
                                </Button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                2
                            </th>
                            <td>
                                Name: ABC<br></br>
                                City: Chennai<br></br>
                                Cuisine: Chinese<br></br>
                                Phone number: 1789023456
                            </td>
                            <td>
                                <Button
                                    className="book-table-btn"
                                    onClick={_ => {
                                    props.setPage(1);
                                    }}
                                >
                                    Book Now
                                </Button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                3
                            </th>
                            <td>
                                Name: VKC<br></br>
                                City: Bangalore<br></br>
                                Cuisine: Korean<br></br>
                                Phone number: 1562347890
                            </td>
                            <td>
                                <Button
                                    className="book-table-btn"
                                    onClick={_ => {
                                    props.setPage(1);
                                    }}
                                >
                                    Book Now
                                </Button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                4
                            </th>
                            <td>
                                Name: Barbeque<br></br>
                                City: Chennai<br></br>
                                Cuisine: Indian<br></br>
                                Phone number: 4561789023
                            </td>
                            <td>
                                <Button
                                    className="book-table-btn"
                                    onClick={_ => {
                                    props.setPage(1);
                                    }}
                                >
                                    Book Now
                                </Button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                5
                            </th>
                            <td>
                                Name: Cafe<br></br>
                                City: Bangalore<br></br>
                                Cuisine: Korean<br></br>
                                Phone number: 4781562390
                            </td>
                            <td>
                                <Button
                                    className="book-table-btn"
                                    onClick={_ => {
                                    props.setPage(1);
                                    }}
                                >
                                    Book Now
                                </Button>
                            </td>
                        </tr>
                    </tbody>
                    </Table>
                </Col>
            </Row>
            
        </div>
    )

}