import React, { useState, useEffect } from "react";
import { Row, Col, Button, FormGroup, Label, Input} from "reactstrap";

export default props => {
  const [user, setUser] = useState({
    email: "",
    Username: "",
    password: ""
  });
  const newuser = async _ => {
    if (
      (user.email.length === 0) |
      (user.Username.length === 0) |
      (user.password.length === 0) 
    ){
      console.log("Incomplete Details")
      props.setPage(3);
    }
    else{
      let res = await fetch("http://localhost:4000/user", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          ...user,
          email: user.email,
          Username: user.Username,
          password: user.password
        })
      });
      res = await res.text();
      console.log("Reserved: " + res);
      props.setPage(4);
    }
  }
  useEffect(() => {
    
  });
  return (
    <div>
      <Row noGutters className="text-center align-items-center pizza-cta">
        <Col>
          <p className="looking-for-pizza">
            Visit our website for a delicious food
          </p>
        </Col>
      </Row>
      <Row noGutters className="text-center big-img-container">
        <Col>
          <img
            src={require("../images/restaurant_image.jpeg")}
            alt="restaurant_image"
            className="big-img"
          />
        </Col>
        <Col sm = {4}>
          <br></br>
          <br></br>
          <form>
          <FormGroup>
            <Label for="exampleEmail">
            Email
            </Label>
            <Input
            id="exampleEmail"
            name="email"
            placeholder="User email"
            type="email"
            bsSize="sm"
            size="sm"
            value={user.email}
            onChange={e => {
              setUser({
                ...user,
                email: e.target.value
              });
            }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleText">
              Username
            </Label>
            <Input
              id="Username"
              name="Username"
              placeholder="Username"
              bsSize="sm"
              size="sm"
              value={user.Username}
              onChange={e => {
                setUser({
                  ...user,
                  Username: e.target.value
                });
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">
              Password
            </Label>
            <Input
              id="examplePassword"
              name="password"
              placeholder="Password"
              type="password"
              bsSize="sm"
              size="sm"
              value={user.password}
              onChange={e => {
                setUser({
                  ...user,
                  password: e.target.value
                });
              }}
            />
          </FormGroup>
          </form>
          <Button
            className="book-table-btn"
            onClick={_ => {
              newuser();
            }}
          >
            Register
          </Button>
          <br></br>
          <br></br>
        </Col>
      </Row>
    </div>
  );
};