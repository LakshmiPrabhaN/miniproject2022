//import React from "react";
import { Row, Col, Button, FormGroup, Label, Input, ListGroupItemHeading} from "reactstrap";
import React, { useState, useEffect } from "react";
import axios from 'axios'
export default props => {
  const [loginF, setLogin] = useState({
    Username: "",
    password: ""
  });

 function Login(){
  
axios.post('http://localhost:4000/login/login',loginF).then((res)=>{
  console.log(res)
  if(res.data=='OK'){
    props.setPage(4);
  }
})


  }
  return (
    <div>
      <Row noGutters className="text-center align-items-center pizza-cta">
        <Col>
          <p className="looking-for-pizza">
            Visit our website for a delicious food
          </p>
        </Col>
      </Row>
      <Row noGutters className="text-center big-img-container">
        <Col>
          <img
            src={require("../images/restaurant_image.jpeg")}
            alt="restaurant_image"
            className="big-img"
          />
        </Col>
        <Col sm = {4}>
          <br></br>
          <br></br>
          <br></br>
          <form>
          <FormGroup>
            <Label for="exampleText">
              Username
            </Label>
            <Input
              id="Username"
              name="Username"
              placeholder="Username"
              bsSize="sm"
              size="sm"
              value={loginF.Username}
            onChange={e => {
              setLogin({
                ...loginF,
                Username: e.target.value
              });
            }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">
              Password
            </Label>
            <Input
              id="examplePassword"
              name="password"
              placeholder="password"
              type="password"
              bsSize="sm"
              size="sm"
              value={loginF.password}
            onChange={e => {
              setLogin({
                ...loginF,
                password: e.target.value
              });
            }}
            />
          </FormGroup>
          </form>
          <Button
            className="book-table-btn"
            onClick={Login}
          >
            Login
          </Button>
          <br></br>
          <br></br>
          <p>New user? Click below to Register</p>
          <Button
            className="book-table-btn"
            onClick={_ => {
              props.setPage(3);
            }}
          >
            Register
          </Button>
        </Col>
      </Row>
    </div>
  );
};