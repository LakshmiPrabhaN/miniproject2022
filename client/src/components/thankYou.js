import React from "react";
import { Row, Col } from "reactstrap";

export default _ => {
  return (
    <div>
      <Row noGutters className="text-center">
        <Col>
          <p className="thanks-subtext">
            You will receive an email with the details of your reservation.
          </p>
        </Col>
      </Row>
      <Row noGutters className="text-center">
        <Col>
          <img
            src={require("../images/thank_you.jpeg")}
            alt="thank_you"
            width="400" height="400"
          />
        </Col>
      </Row>
    </div>
  );
};