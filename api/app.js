require("dotenv").config();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require("mongoose");
const cors = require("cors");

mongoose.connect('mongodb+srv://lakshmiprabha:lakshu@miniproject.dxltyly.mongodb.net/?retryWrites=true&w=majority',(err)=>
console.log('connected successfully'))

var db = mongoose.connection;

var app = express();
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/availability', require('./routes/availabilityRoute'));
app.use('/reservation', require('./routes/reservationRoute'));
app.use('/user', require('./routes/userRoute'));
app.use('/login',require('./routes/userRoute'))

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", _ => {
  console.log("Connected to DB");
});
module.exports = app;
